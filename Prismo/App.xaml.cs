﻿using Prismo.Model;
using System.Windows;

namespace Prismo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static User LoggedInEmployee { get; set; }
    }
}
