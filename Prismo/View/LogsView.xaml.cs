﻿using Prismo.Utility;
using System.Windows.Controls;

namespace Prismo.View
{
    /// <summary>
    /// Interaction logic for LogsView.xaml
    /// </summary>
    public partial class LogsView : UserControl
    {
        public LogsView()
        {
            InitializeComponent();
            this.DataContext = ViewModelLocator.LogsViewModel;
        }
    }
}
