﻿using Prismo.Utility;
using System.Windows.Controls;

namespace Prismo.View
{
    /// <summary>
    /// Interaction logic for PrisonView.xaml
    /// </summary>
    public partial class PrisonView : UserControl
    {
        public PrisonView()
        {
            InitializeComponent();
            this.DataContext = ViewModelLocator.PrisonViewModel;

        }
    }
}
