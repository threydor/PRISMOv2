﻿using Prismo.Utility;
using System.Windows.Controls;

namespace Prismo.View
{
    public partial class UserProfileView : UserControl
    {
        public UserProfileView()
        {
            InitializeComponent();
            this.DataContext = ViewModelLocator.UserProfileViewModel;
        }
    }
}
