﻿using Prismo.Utility;
using System.Windows.Controls;

namespace Prismo.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
            this.DataContext = ViewModelLocator.LoginViewModel;
        }
    }
}
