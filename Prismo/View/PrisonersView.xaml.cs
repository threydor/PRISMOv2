﻿using Prismo.Utility;
using System.Windows.Controls;

namespace Prismo.View
{
    /// <summary>
    /// Interaction logic for PrisonersView.xaml
    /// </summary>
    public partial class PrisonersView : UserControl
    {
        public PrisonersView()
        {
            InitializeComponent();
            this.DataContext = ViewModelLocator.PrisonersViewModel;
        }
    }
}
