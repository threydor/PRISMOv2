﻿using MahApps.Metro.Controls;

namespace Prismo.View.Dialogs
{
    /// <summary>
    /// Interaction logic for PrisonerView.xaml
    /// </summary>
    public partial class PrisonerView : MetroWindow
    {
        public PrisonerView()
        {
            InitializeComponent();
        }
    }
}
