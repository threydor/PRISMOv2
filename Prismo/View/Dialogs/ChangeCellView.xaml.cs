﻿using MahApps.Metro.Controls;

namespace Prismo.View.Dialogs
{
    /// <summary>
    /// Interaction logic for ChangeCellView.xaml
    /// </summary>
    public partial class ChangeCellView : MetroWindow
    {
        public ChangeCellView()
        {
            InitializeComponent();
        }
    }
}
