﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Prismo.Extensions
{
    class DayRangeGroup:IValueConverter
    {
        public int GroupInterval { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int days = int.Parse(value.ToString());
            if (days < GroupInterval)
            {
                return $"1 - {GroupInterval}";
            }
            else
            {
                GroupInterval = 100;
                int interval = days / GroupInterval;
                int lowerLimit = interval * GroupInterval;
                int upperLimit = (interval + 1) * GroupInterval;
                return $"{lowerLimit} – {upperLimit - 1}";
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
