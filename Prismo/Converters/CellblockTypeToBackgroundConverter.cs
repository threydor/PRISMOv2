﻿using Prismo.Model;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;


namespace Prismo.Converters
{
    class CellblockTypeToBackgroundConverter : IValueConverter
    {
        public Brush RegularBrush { get; set; }
        public Brush RestrictedBrush { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((CellblockType)value == CellblockType.Zwykły)
            {
                return RegularBrush;
            }
            else
            {
                return RestrictedBrush;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
