﻿using Prismo.Model;
using System.Collections.Generic;
using System.Linq;

namespace Prismo.Utility.Service
{
    interface ICellDataService
    {
        List<Prisoner> GetAllPrisonersFromCell(Cell cell);
        List<Cell> GetAllCells();
        void AddOrUpdatePrisoner(Cell cell, Prisoner prisoner);
        void DeletePrisoner(Cell cell, Prisoner prisoner);
        void UpdateCell(Cell cell);
        Prisoner GetPrisonerById(int id);
    }

    class CellDataService : ICellDataService
    {
        public List<Prisoner> GetAllPrisonersFromCell(Cell cell)
        {
            return CellFlyweight.GetInstance.GetPrisonersFromCell(cell);
        }
        public List<Cell> GetAllCells()
        {
            return Data.LoadCells();
        }
        public void AddOrUpdatePrisoner(Cell cell, Prisoner prisoner)
        {
            cell.AddNewPrisoner(prisoner);
            Data.SaveCells();
        }
        public void DeletePrisoner(Cell cell, Prisoner prisoner)
        {
            cell.RemovePrisoner(prisoner);
            Data.SaveCells();
        }

        public void UpdateCell(Cell cell)
        {

            var cellToUpdate = Data.CellsList.FirstOrDefault(x => x.Id == cell.Id);
            if (cellToUpdate != null)
            {
                cellToUpdate.BlockId = cell.BlockId;
                cellToUpdate.BlockName = cell.BlockName;
                cellToUpdate.Population = cell.Population;
                cellToUpdate.Size = cell.Size;
                Data.SaveCells();
            }
        }

        public Prisoner GetPrisonerById(int id)
        {
            return Data.Prisoners.FirstOrDefault(p => p.Id == id);
        }
    }
}
