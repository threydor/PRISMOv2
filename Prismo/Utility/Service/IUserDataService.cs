﻿using Prismo.Model;
using System.Collections.Generic;
using System.Linq;

namespace Prismo.Utility.Service
{
    interface IUserDataService
    {
        List<User> GetAllUsers();
        void AddUser(User user);
        void AddOrUpdateUser(User user);
        User GetUserById(int id);
    }

    class UserDataService : IUserDataService
    {
        public List<User> GetAllUsers()
        {
            return Data.Users.ToList();
        }
        public void AddUser(User user)
        {
            Data.Users.Add(user);
            Data.SaveUsers();
        }

        public void AddOrUpdateUser(User user)
        {
            if (user.Id == 0)
            {
                user.Id = Data.Users.Count + 1;
                Data.Users.Add(user);
            }
            Data.SaveUsers();
        }

        public User GetUserById(int id)
        {
            return Data.Users.FirstOrDefault(p => p.Id == id);
        }
    }
}
