﻿using Prismo.Model;
using System.Collections.Generic;
using System.Linq;

namespace Prismo.Utility.Service
{
    interface IPrisonerDataService
    {
        List<Prisoner> GetAllPrisoners();
        void AddOrUpdatePrisoner(Prisoner prisoner);
        void DeletePrisoner(Prisoner prisoner);
        Prisoner GetPrisonerById(int id);
    }

    class PrisonerDataService : IPrisonerDataService
    {
        public List<Prisoner> GetAllPrisoners()
        {
            return Data.Prisoners.ToList();
        }
        public void AddOrUpdatePrisoner(Prisoner prisoner)
        {
            if (prisoner.Id == 0)
            {
                prisoner.Id = Data.Prisoners.Count + 1;
                Data.Prisoners.Add(prisoner);
            }
            Data.SavePrisoners();
        }
        public void DeletePrisoner(Prisoner prisoner)
        {
            Data.Prisoners.Remove(prisoner);
            Data.SavePrisoners();
        }

        public Prisoner GetPrisonerById(int id)
        {
            return Data.Prisoners.FirstOrDefault(p => p.Id == id);
        }
    }
}
