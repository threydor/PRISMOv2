﻿namespace Prismo.Utility
{
    public class UpdateListMessage
    {
        public string Parameter { get; set; }
        public int Id { get; set; }

        public UpdateListMessage(string parameter, int id)
        {
            Parameter = parameter;
            Id = id;
        }

        public UpdateListMessage()
        {
        }
    }
}
