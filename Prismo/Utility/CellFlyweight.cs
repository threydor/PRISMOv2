﻿using Prismo.Model;
using Prismo.Utility.Service;
using System.Collections.Generic;
using System.Linq;

namespace Prismo.Utility
{
    class CellFlyweight
    {
        private static CellFlyweight _flyweightInstance;
        private IPrisonerDataService _prisonerDataService = new PrisonerDataService();
        private ICellDataService _cellDataService = new CellDataService();

        public Dictionary<Prisoner, Cell> CellDictionary { get; set; }
        private CellFlyweight()
        {
            this.CellDictionary = new Dictionary<Prisoner, Cell>(new PrisonerComparer());
        }
        public static CellFlyweight GetInstance
        {
            get
            {
                if (_flyweightInstance == null)
                {
                    _flyweightInstance = new CellFlyweight();
                }
                return _flyweightInstance;
            }
        }

        public Cell GetCellByPrisoner(int prisonerId)
        {
            foreach (var entry in CellDictionary)
            {
                if (entry.Key.Id == prisonerId)
                {
                    return entry.Value;
                }
            }
            return null;
        }
        public List<Prisoner> GetPrisonersFromCell(Cell cell)
        {
            var result = new List<Prisoner>();
            foreach (KeyValuePair<Prisoner, Cell> entry in CellDictionary)
            {
                if (entry.Value.Id == cell.Id)
                {
                    result.Add(_prisonerDataService.GetPrisonerById(entry.Key.Id));
                }
            }
            cell.Population = result.Count;
            new CellDataService().UpdateCell(cell);
            return result;
        }
        public void AddPrisonerToCell(Prisoner prisoner, Cell cell)
        {
            this.CellDictionary.Add(prisoner, cell);
            Data.SaveCellFlyweight();
        }
        public void RemovePrisonerFromCell(Prisoner prisoner)
        {
            this.CellDictionary.Remove(prisoner);
            Data.SaveCellFlyweight();
            Data.SaveCells();
        }
        public void RemoveCell(Cell cell)
        {
            var comparer = new CellComparer();
            var prisoners = new List<Prisoner>();

            foreach (KeyValuePair<Prisoner, Cell> entry in this.CellDictionary)
            {
                if (comparer.Equals(entry.Value, cell))
                {
                    prisoners.Add(entry.Key);
                }
            }

            foreach (var prisoner in prisoners)
            {
                this.CellDictionary.Remove(prisoner);
            }
            Data.CellsList.Remove(cell);
            Data.SaveCellFlyweight();
        }
        public List<Prisoner> GetPrisonersWithCells()
        {
            List<Prisoner> result = new List<Prisoner>();
            foreach (KeyValuePair<Prisoner, Cell> entry in this.CellDictionary)
            {
                result.Add(entry.Key);
            }
            return result;
        }
        public List<Cell> GetEmptyCells()
        {
            List<Cell> result = new List<Cell>();
            HashSet<Cell> fullCells = new HashSet<Cell>();
            foreach (KeyValuePair<Prisoner, Cell> entry in this.CellDictionary)
            {
                if (!entry.Value.HasFreeSpace())
                    fullCells.Add(entry.Value);
            }
            result = _cellDataService.GetAllCells().Except(fullCells.ToList()).ToList();
            return result;
        }
        public List<Prisoner> GetFreePrisoners()
        {
            var result = new List<Prisoner>();
            var allPrisoners = _prisonerDataService.GetAllPrisoners();
            var prisonersWithCells = GetPrisonersWithCells();
            result = allPrisoners.Except(GetPrisonersWithCells(), new PrisonerComparer()).ToList();
            return result;
        }
        public void LoadData(CellFlyweight data)
        {
            _flyweightInstance = data;
        }

    }
}
