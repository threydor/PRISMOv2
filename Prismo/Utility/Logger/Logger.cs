﻿using Prismo.Model.PersonalLogger;
using System.Collections.Generic;
using System.IO;

namespace Prismo.Utility.Logger
{
    public class Logger
    {
        private int _currentId;

        private static Logger _loggerInstance;

        public static Logger GetInstance
        {
            get
            {
                if (_loggerInstance == null)
                {
                    _loggerInstance = new Logger();
                }
                return _loggerInstance;
            }
        }

        private Dictionary<int, ActionLog> logs;

        private Logger()
        {
            logs = new Dictionary<int, ActionLog>();
        }

        public void AddLog(string info)
        {
            var log = new ActionLog(info);
            logs.Add(_currentId++, log);
        }

        public void AddPersonLog(string info)
        {
            var person = App.LoggedInEmployee;
            var personalLogger = new LogPersonAction(person);
            personalLogger.AddNewLog(new ActionLog(info));
            this.AddLog(info);
        }

        public Dictionary<int, ActionLog> GetLogs()
        {
            return logs;
        }

        public void SaveLogs()
        {
            var writer = new StreamWriter(Data.DataBasePath + "\\logs.txt", true);

            foreach (var log in logs)
            {
                writer.WriteLine(log.Value.ToString());
            }
            writer.Close();
        }
        public void ClearLogs()
        {
            logs.Clear();
            logs.Add(_currentId++, new ActionLog("Wyczyszczono logi poprzednich sesji."));
            var writer = new StreamWriter(Data.DataBasePath + "\\logs.txt", false);
            foreach (var log in logs)
            {
                writer.WriteLine(log.Value.ToString());
            }
            writer.Close();
            logs.Clear();
        }
        public bool FileExists()
        {
            return File.Exists(Data.DataBasePath + "\\logs.txt");
        }
    }
}
