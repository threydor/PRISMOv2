﻿namespace Prismo.Utility
{
    interface ICellCommand
    {
        void Execute();
    }
}
