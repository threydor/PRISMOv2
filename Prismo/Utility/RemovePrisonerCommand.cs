﻿using Prismo.Model;

namespace Prismo.Utility
{
    class RemovePrisonerCommand : ICellCommand
    {
        private Prisoner _prisoner;

        public RemovePrisonerCommand(Prisoner prisoner)
        {
            _prisoner = prisoner;
        }

        public void Execute()
        {
            Logger.Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) usunął więźnia: {_prisoner.Name} {_prisoner.Surname} z jego celi");
            CellFlyweight.GetInstance.RemovePrisonerFromCell(_prisoner);
        }
    }
}
