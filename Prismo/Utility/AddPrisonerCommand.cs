﻿using Prismo.Model;

namespace Prismo.Utility
{
    class AddPrisonerCommand : ICellCommand
    {
        private Cell _cell;
        private Prisoner _prisoner;

        public AddPrisonerCommand(Cell cell, Prisoner prisoner)
        {
            _cell = cell;
            _prisoner = prisoner;
        }
        public void Execute()
        {
            Logger.Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) dodał więźnia: {_prisoner.Name} {_prisoner.Surname} do celi {_cell.Id}");
            CellFlyweight.GetInstance.AddPrisonerToCell(_prisoner, _cell);
        }
    }
}
