﻿using Prismo.View.Dialogs;
using System.Windows;

namespace Prismo.Utility
{
    public class DialogService
    {
        private Window _mainWindow = App.Current.MainWindow;

        private Window _cellView;
        private Window _blockView;
        private Window _prisonerView;
        private Window _prisonerCellView;
        private Window _userView;
        private Window _changeCellView;

        public void ShowBlockDialog()
        {
            _blockView = new BlockView();
            _blockView.Owner = _mainWindow;
            _blockView.ShowDialog();
        }

        public void CloseBlockDialog()
        {
            if (_blockView != null)
                _blockView.Close();
        }

        public void ShowPrisonerDialog()
        {
            _prisonerView = new PrisonerView();
            _prisonerView.Owner = _mainWindow;
            _prisonerView.ShowDialog();
        }

        public void ClosePrisonerDialog()
        {
            if (_prisonerView != null)
                _prisonerView.Close();
        }

        public void ShowCellDialog()
        {
            _cellView = new CellView();
            _cellView.Owner = _mainWindow;
            _cellView.ShowDialog();
        }

        public void CloseCellDialog()
        {
            if (_cellView != null)
                _cellView.Close();
        }

        public void ShowUserDialog()
        {
            _userView = new UserView();
            _userView.Owner = _mainWindow;
            _userView.ShowDialog();
        }

        public void CloseUserDialog()
        {
            if (_userView != null)
                _userView.Close();
        }

        public void ShowPrisonerCellDialog()
        {
            _prisonerCellView = new PrisonerCellView();
            _prisonerCellView.Owner = _mainWindow;
            _prisonerCellView.ShowDialog();
        }

        public void ClosePrisonerCellDialog()
        {
            if (_prisonerCellView != null)
                _prisonerCellView.Close();
        }
      
        public void ShowChangeCellDialog()
        {
            _changeCellView = new ChangeCellView();
            _changeCellView.Owner = _mainWindow;
            _changeCellView.ShowDialog();
        }
        public void CloseChangeCellDialog()
        {
            if (_changeCellView != null)
                _changeCellView.Close();
        }
    }
}
