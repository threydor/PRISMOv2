﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Prismo.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Prismo.Utility
{
    public static class Data
    {
        public static string DataBasePath = AppDomain.CurrentDomain.BaseDirectory + "cfg";
        public static List<User> Users { get; set; }
        public static List<Prisoner> Prisoners { get; set; }
        public static List<Cellblock> BlocksList { get; set; }
        public static List<Cell> CellsList { get; set; }
        public static Dictionary<Prisoner, Cell> CellFlyweightDict { get; set; }

        public static void PrepareDatabase()
        {
            if (!Directory.Exists(DataBasePath))
            {
                Directory.CreateDirectory(DataBasePath);
            }
            Prisoners = LoadPrisoners();
            BlocksList = LoadBlocks();
            CellsList = LoadCells();
            Users = LoadUsers();
        }



        public static bool IsNewDatabase()
        {
            if (!File.Exists(DataBasePath + "\\users.json") || (Users != null && Users.Count == 0))
            {
                JsonInit();
                return true;
            }
            Users = LoadUsers();

            return false;
        }

        private static void JsonInit()
        {
            if (!File.Exists(DataBasePath + "\\users.json"))
            {
                Users = new List<User>();
                SaveUsers();
            }

            if (!File.Exists(DataBasePath + "\\prisoners.json"))
            {
                Prisoners = new List<Prisoner>();
                SavePrisoners();
            }

            if (!File.Exists(DataBasePath + "\\blocks.json"))

            {
                BlocksList = new List<Cellblock>();
                SaveBlocks();
            }

            if (!File.Exists(DataBasePath + "\\cells.json"))
            {
                CellsList = new List<Cell>();
                SaveCells();
            }
            if (!File.Exists(DataBasePath + "\\cellFlyweight.json"))
            {
                CellFlyweightDict = new Dictionary<Prisoner, Cell>();
                SaveCellFlyweight();
            }
        }



        #region Users

        public static List<User> LoadUsers()
        {
            List<User> result;
            if (File.Exists(DataBasePath + "\\users.json"))
            {
                using (var file = File.OpenText(DataBasePath + "\\users.json"))
                {
                    var serializer = new JsonSerializer();
                    result = (List<User>)serializer.Deserialize(file, typeof(List<User>));
                }
            }
            else
            {
                result = new List<User>();
            }
            return result;
        }

        public static void SaveUsers()
        {
            using (var file = File.CreateText(DataBasePath + "\\users.json"))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(file, Users);
            }
        }

        #endregion

        #region Prisoners

        public static List<Prisoner> LoadPrisoners()
        {
            List<Prisoner> result;
            if (File.Exists(DataBasePath + "\\prisoners.json"))
            {
                using (var file = File.OpenText(DataBasePath + "\\prisoners.json"))
                {
                    var serializer = new JsonSerializer();
                    result = (List<Prisoner>)serializer.Deserialize(file, typeof(List<Prisoner>));
                }
            }
            else
            {
                result = new List<Prisoner>();
            }
            return result;
        }

        public static void SavePrisoners()
        {
            using (var file = File.CreateText(DataBasePath + "\\prisoners.json"))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(file, Prisoners);
            }
        }

        #endregion

        #region Cellblocks
        public static List<Cellblock> LoadBlocks()
        {
            List<Cellblock> result;
            if (File.Exists(DataBasePath + "\\blocks.json"))
            {
                using (var file = File.OpenText(DataBasePath + "\\blocks.json"))
                {
                    var serializer = new JsonSerializer();
                    result = (List<Cellblock>)serializer.Deserialize(file, typeof(List<Cellblock>));
                }
                //LoadCells();
                //foreach (var cellblock in BlocksList)
                //{
                //    // cellblock.Cells = CellsList.Where(x => x.BlockId == cellblock.Id).ToList();
                //}
            }
            else
            {
                result = new List<Cellblock>();
            }
            return result;
        }


        public static void AddBlock(Cellblock newBlock)
        {
            BlocksList.Add(newBlock);
            SaveBlocks();
        }

        public static void DeleteBlock(Cellblock block)
        {
            var blockToRemove = BlocksList.FirstOrDefault(x => x.Id == block.Id);
            if (blockToRemove != null)
            {
                BlocksList.Remove(blockToRemove);
                blockToRemove.Cells = LoadCells().Where(x => x.BlockId == blockToRemove.Id).ToList();
                foreach (var cell in blockToRemove.Cells)
                {
                    DeleteCell(cell);
                }
                SaveBlocks();
            }
        }

        public static void UpdateBlock(Cellblock block)
        {
            var blockToUpdate = BlocksList.FirstOrDefault(x => x.Id == block.Id);

            if ( blockToUpdate != null && blockToUpdate.Name != block.Name)
            {
                blockToUpdate.Name = block.Name;
                CellsList = LoadCells();
                var cellsToEdit = CellsList.Where(x => x.BlockId == block.Id);
                foreach (var cell in cellsToEdit)
                {
                    cell.BlockName = block.Name;
                }
                SaveCells();
                //foreach (var cell in CellFlyweight.GetInstance.CellDictionary.Values.Where(x => x.BlockId == block.Id))
                //{
                //    cell.BlockName = block.Name;
                //}
                SaveCellFlyweight();
                blockToUpdate.Type = block.Type;
            }
            SaveBlocks();
        }

        public static void SaveBlocks()
        {
            using (var file = File.CreateText(DataBasePath + "\\blocks.json"))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(file, BlocksList);
            }
        }
        #endregion
        #region CellsFlyweight
        public static void SaveCellFlyweight()
        {

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new DictionaryAsArrayResolver();
            string json;
            using (var file = File.CreateText(DataBasePath + "\\cellFlyweight.json"))
            {
                var serializer = new JsonSerializer();
                json = JsonConvert.SerializeObject(CellFlyweight.GetInstance, settings);
            }
            File.WriteAllText(DataBasePath + "\\cellFlyweight.json", json);

        }
        public static void LoadCellFlyweight()
        {
            if (File.Exists(DataBasePath + "\\cellFlyweight.json"))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.Formatting = Formatting.Indented;
                settings.ContractResolver = new DictionaryAsArrayResolver();
                using (var file = File.OpenText(DataBasePath + "\\cellFlyweight.json"))
                {
                    var serializer = new JsonSerializer();
                    CellFlyweight.GetInstance.LoadData(JsonConvert.DeserializeObject<CellFlyweight>(file.ReadToEnd(), settings));
                }
            }
        }
        #endregion
        #region Cells
        public static void LoadCellsData()
        {
            List<Cell> result;
            if (File.Exists(DataBasePath + "\\cells.json"))
            {
                using (var file = File.OpenText(DataBasePath + "\\cells.json"))
                {
                    var serializer = new JsonSerializer();
                    result = (List<Cell>)serializer.Deserialize(file, typeof(List<Cell>));
                }
            }
            else
            {
                result = new List<Cell>();
            }
            CellsList = result;
        }
        public static List<Cell> LoadCells(int blockId)
        {
            List<Cell> result;
            if (File.Exists(DataBasePath + "\\cells.json"))
            {
                using (var file = File.OpenText(DataBasePath + "\\cells.json"))
                {
                    var serializer = new JsonSerializer();
                    result = (List<Cell>)serializer.Deserialize(file, typeof(List<Cell>));
                    result = result.Where(x => x.BlockId == blockId).ToList<Cell>();
                }
            }
            else
            {
                result = new List<Cell>();
            }
            return result;
        }
        internal static List<Cell> LoadCells()
        {
            List<Cell> result;
            if (File.Exists(DataBasePath + "\\cells.json"))
            {
                using (var file = File.OpenText(DataBasePath + "\\cells.json"))
                {
                    var serializer = new JsonSerializer();
                    result = (List<Cell>)serializer.Deserialize(file, typeof(List<Cell>));
                }
            }
            else
            {
                result = new List<Cell>();
            }
            return result;
        }

        public static void AddCell(Cell newCell)
        {
            if (CellsList != null)
                CellsList.Add(newCell);
            else
            {
                CellsList = new List<Cell>();
                CellsList.Add(newCell);
            }
            SaveCells();
        }

        public static void DeleteCell(Cell cell)
        {
            if (cell != null)
            {
                var cellToRemove = CellsList.FirstOrDefault(x => x.Id == cell.Id);
                if (cellToRemove != null)
                {
                    CellsList.Remove(cellToRemove);
                    CellFlyweight.GetInstance.RemoveCell(cellToRemove);
                }
            }
            SaveCells();
        }

        public static void SaveCells()
        {
            using (var file = File.CreateText(DataBasePath + "\\cells.json"))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(file, CellsList);
            }
        }
        #endregion
    }
    class DictionaryAsArrayResolver : DefaultContractResolver
    {
        protected override JsonContract CreateContract(Type objectType)
        {
            if (objectType.GetInterfaces().Any(i => i == typeof(IDictionary) ||
                (i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IDictionary<,>))))
            {
                return base.CreateArrayContract(objectType);
            }

            return base.CreateContract(objectType);
        }
    }

}
