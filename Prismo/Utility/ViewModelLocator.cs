﻿using Prismo.ViewModel;

namespace Prismo.Utility
{
    public class ViewModelLocator
    {

        public static MainWindowViewModel MainWindowViewModel { get; } = new MainWindowViewModel();
        public static LoginViewModel LoginViewModel { get; } = new LoginViewModel();
        public static CellViewModel CellViewModel { get; } = new CellViewModel();
        public static BlockViewModel BlockViewModel { get; } = new BlockViewModel();
        public static PrisonViewModel PrisonViewModel { get; } = new PrisonViewModel();
        public static PrisonerCellViewModel PrisonerCellViewModel { get; } = new PrisonerCellViewModel();
        public static PrisonersViewModel PrisonersViewModel { get; } = new PrisonersViewModel();
        public static PrisonerViewModel PrisonerViewModel { get; } = new PrisonerViewModel();
        public static LogsViewModel LogsViewModel { get; } = new LogsViewModel();
        public static UserViewModel UserViewModel { get; } = new UserViewModel();
        public static UserProfileViewModel UserProfileViewModel { get; } = new UserProfileViewModel();
        public static ChangeCellViewModel ChangeCellViewModel { get; } = new ChangeCellViewModel();
    }
}
