﻿using Prismo.Extensions;
using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using Prismo.Utility.Service;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class PrisonViewModel : INotifyPropertyChanged
    {
        // private readonly CellDataService _cellDataService;
        private readonly IUserDataService _userDataService = new UserDataService();
        private readonly DialogService _dialogService = new DialogService();

        private ObservableCollection<Cellblock> _blocks;
        public ObservableCollection<Cellblock> Blocks
        {
            get { return _blocks; }
            set
            {
                _blocks = value;
                OnPropertyChanged(nameof(Blocks));
            }
        }

        private ObservableCollection<Cell> _cells;
        public ObservableCollection<Cell> Cells
        {
            get { return _cells; }
            set
            {
                if (_cells != value)
                {
                    _cells = value;
                    OnPropertyChanged("Cells");
                }
            }
        }

        private ObservableCollection<Prisoner> _prisoners;
        public ObservableCollection<Prisoner> Prisoners
        {
            get { return _prisoners; }
            set
            {
                if (_prisoners != value)
                {
                    _prisoners = value;
                    OnPropertyChanged("Prisoners");
                }
            }
        }

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                _users = value;
                OnPropertyChanged(nameof(Users));
            }
        }

        private Cell _selectedCell;
        public Cell SelectedCell
        {
            get { return _selectedCell; }
            set
            {
                if (_selectedCell != value)
                {
                    _selectedCell = value;
                    LoadPrisoners();
                    OnPropertyChanged("SelectedCell");
                }
            }
        }


        private Cellblock _selectedBlock;
        public Cellblock SelectedBlock
        {
            get { return _selectedBlock; }
            set
            {
                if (_selectedBlock != value)
                {
                    _selectedBlock = value;
                    LoadCells();

                    LoadPrisoners();
                    OnPropertyChanged("SelectedBlock");
                }
            }
        }

        private Prisoner _selectedPrisoner;
        public Prisoner SelectedPrisoner
        {
            get { return _selectedPrisoner; }
            set
            {
                if (_selectedPrisoner != value)
                {
                    _selectedPrisoner = value;
                    OnPropertyChanged("SelectedPrisoner");
                }
            }
        }

        private User _selectedUser;
        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                if (_selectedUser != value)
                {
                    _selectedUser = value;
                    OnPropertyChanged("SelectedUser");
                }
            }
        }

        public Visibility AdminTabVisibility
        {
            get
            {
                if (App.LoggedInEmployee != null && App.LoggedInEmployee.Perms == UserPerms.Admin)
                    return Visibility.Visible;
                return Visibility.Collapsed;
            }
        }

        public ICommand NewBlock { get; set; }
        public ICommand EditBlockCommand { get; set; }
        public ICommand DeleteBlockCommand { get; set; }
        public ICommand NewCell { get; set; }
        public ICommand EditCellCommand { get; set; }
        public ICommand DeleteCellCommand { get; set; }
        public ICommand AddUserCommand { get; set; }
        public ICommand EditUserCommand { get; set; }
        public ICommand AddToCellCommand { get; set; }
        public ICommand RemovePrisonerFromCellCommand { get; set; }
        public ICommand PrintUserProfileCommand { get; set; }

        public PrisonViewModel()
        {
            NewBlock = new RelayCommand(AddBlock, CanAddBlock);
            EditBlockCommand = new RelayCommand(EditBlock, IsBlockSelected);
            DeleteBlockCommand = new RelayCommand(DeleteBlock, IsBlockSelected);
            NewCell = new RelayCommand(AddCell, CanAddCell);
            EditCellCommand = new RelayCommand(EditCell, CanEditCell);
            AddUserCommand = new RelayCommand(AddUser, CanAddUser);
            EditUserCommand = new RelayCommand(EditUser, CanEditOrPrintUser);
            PrintUserProfileCommand = new RelayCommand(PrintUser, CanEditOrPrintUser);
            AddToCellCommand = new RelayCommand(AddToCell, CanAddToCell);
            RemovePrisonerFromCellCommand = new RelayCommand(RemoveFromCell, CanRemoveFromCell);
            DeleteCellCommand = new RelayCommand(DeleteCell, CanDeleteCell);
            LoadData();
            Messenger.Default.Register<UpdateListMessage>(this, OnUpdateListMessageReceived);
        }

        private void PrintUser(object obj)
        {
            if (SelectedUser != null)
                SelectedUser.Print();
        }

        private void DeleteBlock(object obj)
        {
            SelectedBlock.Cells = Data.LoadCells().Where(x => x.BlockId == SelectedBlock.Id).ToList();
            var msg = string.Empty;
            if (SelectedBlock.Cells.Any(cell => cell.IsPopulated()))
            {
                msg = "Blok posiada celę z przypisanymi więźniami! ";
            }
            msg += "Czy na pewno chcesz usunąć blok?";

            var messageBoxResult = MessageBox.Show(msg, "Potwierdzenie", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) usunął blok: [{SelectedBlock.Id}] {SelectedBlock.Name} ({_selectedBlock.Type})");
                Data.DeleteBlock(SelectedBlock);
                LoadData();
            }
        }

        private void EditBlock(object obj)
        {
            Messenger.Default.Send(_selectedBlock);
            _dialogService.ShowBlockDialog();
        }

        private bool IsBlockSelected(object obj)
        {
            return SelectedBlock != null;
        }

        private void EditCell(object obj)
        {
            Messenger.Default.Send(_selectedCell);
            _dialogService.ShowCellDialog();
        }

        private bool CanDeleteCell(object obj)
        {
            return SelectedCell != null;
        }

        private void DeleteCell(object obj)
        {
            var msg = string.Empty;
            if (SelectedCell.IsPopulated())
            {
                msg = "Cela ma przypisanych więźniów! ";
            }
            msg += "Czy na pewno chcesz usunąć celę?";

            var messageBoxResult = MessageBox.Show(msg, "Potwierdzenie", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) usunął celę: {SelectedCell.Id} z bloku {SelectedCell.BlockName}");
                Data.DeleteCell(SelectedCell);
                LoadCells();
                LoadPrisoners();
            }
        }

        private bool CanRemoveFromCell(object obj)
        {
            return SelectedPrisoner != null;
        }

        private void RemoveFromCell(object obj)
        {
            new RemovePrisonerCommand(SelectedPrisoner).Execute();
            if (SelectedCell != null)
            {
                Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) usunął więźnia: {SelectedPrisoner.Id} - {SelectedPrisoner.Name} {SelectedPrisoner.Surname} z celi {SelectedCell.Id}");
                CellFlyweight.GetInstance.GetPrisonersFromCell(SelectedCell);
                var cell = SelectedCell;
                LoadCells();
                LoadPrisoners();
                SelectedCell = Cells.FirstOrDefault(x => x.Id == cell.Id);
            }
        }

        private void OnUpdateListMessageReceived(UpdateListMessage obj)
        {
            if (obj.Parameter == "Cell")
            {
                if (SelectedBlock != null)
                {
                    LoadCells();
                    LoadPrisoners();
                    _dialogService.CloseCellDialog();
                    SelectedCell = Cells.FirstOrDefault(x => x.Id == obj.Id);
                    _dialogService.ClosePrisonerCellDialog();
                }

            }
            else if (obj.Parameter == "Block")
            {
                LoadData();
                _dialogService.CloseBlockDialog();
                SelectedBlock = Blocks.FirstOrDefault(x => x.Id == obj.Id);
            }
            else
            {
                LoadData();
                _dialogService.CloseUserDialog();
                _dialogService.CloseBlockDialog();
            }

        }

        private void LoadData()
        {
            Blocks = Data.LoadBlocks().ToObservableCollection();
            LoadCells();
            LoadPrisoners();
            Users = _userDataService.GetAllUsers().ToObservableCollection();
        }

        private void LoadCells()
        {
            if (SelectedBlock != null)
                Cells = Data.LoadCells(SelectedBlock.Id).ToObservableCollection();
            else
            {
                var cells = new List<Cell>();
                Cells = cells.ToObservableCollection();
            }
        }

        private void LoadPrisoners()
        {
            if (SelectedCell != null && !string.IsNullOrEmpty(SelectedCell.BlockName))
                Prisoners = CellFlyweight.GetInstance.GetPrisonersFromCell(SelectedCell).ToObservableCollection();
            else
            {
                var prisoners = new List<Prisoner>();
                Prisoners = prisoners.ToObservableCollection();
            }
        }


        private bool CanAddCell(object obj)
        {
            if (SelectedBlock != null)
            {
                return true;
            }
            return false;
        }

        private bool CanEditCell(object obj)
        {
            if (SelectedCell != null)
            {
                return true;
            }
            return false;
        }
        private bool CanAddToCell(object obj)
        {
            if (SelectedCell != null && SelectedCell.HasFreeSpace())
            {
                return true;
            }
            return false;
        }

        private void AddToCell(object obj)
        {
            Messenger.Default.Send(SelectedCell);
            _dialogService.ShowPrisonerCellDialog();
        }

        private void AddCell(object obj)
        {
            var cell = new Cell(4);
            cell.BlockId = SelectedBlock.Id;
            cell.BlockName = SelectedBlock.Name;
            var newId = 0;
            if (Data.CellsList != null && Data.CellsList.Count != 0)
            {
                newId = (Data.CellsList.OrderByDescending(x => x.Id).First()).Id + 1;
            }
            cell.Id = newId;
            Messenger.Default.Send(cell);
            _dialogService.ShowCellDialog();
        }

        private bool CanAddBlock(object obj) => true;
        private void AddBlock(object obj)
        {
            var block = new Cellblock();
            var newId = 0;
            if (Blocks.Count != 0)
            {
                newId = (Blocks.OrderByDescending(x => x.Id).First()).Id + 1;
            }
            block.Id = newId;
            Messenger.Default.Send(block);
            _dialogService.ShowBlockDialog();
            LoadCells();
        }

        private bool CanAddUser(object obj)
        {
            return App.LoggedInEmployee.Perms == UserPerms.Admin;
        }
        private void AddUser(object obj)
        {
            Messenger.Default.Send(new User());
            _dialogService.ShowUserDialog();
        }

        private bool CanEditOrPrintUser(object obj)
        {
            return SelectedUser != null;
        }
        private void EditUser(object obj)
        {
            Messenger.Default.Send(SelectedUser);
            _dialogService.ShowUserDialog();
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
