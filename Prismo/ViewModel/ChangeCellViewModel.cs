﻿using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class ChangeCellViewModel
    {
        private Cell _selectedCell;
        public List<Cell> Cells { get; set; }
        public Prisoner Prisoner { get; set; }

        public ICommand SaveCommand { get; set; }
        public ChangeCellViewModel()
        {
            Messenger.Default.Register<Prisoner>(this, OnPrisonerReceived);
            SaveCommand = new RelayCommand(Save, CanSaveCell);
        }
        private bool CanSaveCell(object obj)
        {
            return true;
        }

        private void Save(object obj)
        {
            var cell = CellFlyweight.GetInstance.GetCellByPrisoner(Prisoner.Id);

            if (cell != null)
            {
                CellFlyweight.GetInstance.RemovePrisonerFromCell(Prisoner);
            }

            CellFlyweight.GetInstance.AddPrisonerToCell(Prisoner, SelectedCell);
            CellFlyweight.GetInstance.GetPrisonersFromCell(SelectedCell);
            Data.SaveCellFlyweight();
            Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) zmienił celę więźnia: {Prisoner.Name} {Prisoner.Surname} na: {SelectedCell.Id}");
            Messenger.Default.Send(new UpdateListMessage());
        }
        public Cell SelectedCell
        {
            get { return _selectedCell; }
            set
            {
                if (_selectedCell != value)
                {
                    _selectedCell = value;
                    OnPropertyChanged("SelectedCell");
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPrisonerReceived(Prisoner prisoner)
        {
            Cells = new List<Cell>();
            Cells = Data.LoadCells().Where(x => x.HasFreeSpace()).ToList();
            Prisoner = prisoner;
        }


        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
