﻿using Prismo.Extensions;
using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Service;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class PrisonerCellViewModel : INotifyPropertyChanged
    {
        private ICellDataService _cellDataService = new CellDataService();
        private Cell _cell;
        private int _tempVacancies;
        private List<ICellCommand> _commands;


        private Prisoner _prisonerToAdd;
        public Prisoner PrisonerToAdd
        {
            get { return _prisonerToAdd; }
            set
            {
                if (_prisonerToAdd != value)
                {
                    _prisonerToAdd = value;
                    OnPropertyChanged("PrisonerToAdd");
                }
            }
        }
        private Prisoner _prisonerToRemove;
        public Prisoner PrisonerToRemove
        {
            get { return _prisonerToRemove; }
            set
            {
                if (_prisonerToRemove != value)
                {
                    _prisonerToRemove = value;
                    OnPropertyChanged("PrisonerToRemove");
                }
            }
        }
        public PrisonerCellViewModel()
        {
            _commands = new List<ICellCommand>();
            Messenger.Default.Register<Cell>(this, OnCellReceived);
            SaveCommand = new RelayCommand(Save, CanSave);
            AddCommand = new RelayCommand(AddToCell, CanAddToCell);
            RemoveCommand = new RelayCommand(Remove, CanRemove);
            SelectedPrisoners = new ObservableCollection<Prisoner>();
            LoadData();
        }

        private bool CanRemove(object obj)
        {
            if (PrisonerToRemove != null)
            {
                return true;
            }
            return false;
        }

        private void Remove(object obj)
        {
            _tempVacancies++;
            _commands.Add(new RemovePrisonerCommand(PrisonerToRemove));
            _prisoners.Add(PrisonerToRemove);

            _selectedPrisoners.Remove(PrisonerToRemove);
        }

        private bool CanAddToCell(object obj)
        {
            if (PrisonerToAdd != null && _tempVacancies > 0)
            {
                return true;
            }
            return false;
        }

        private void AddToCell(object obj)
        {
            _commands.Add(new AddPrisonerCommand(_cell, PrisonerToAdd));
            _selectedPrisoners.Add(PrisonerToAdd);
            _prisoners.Remove(PrisonerToAdd);
            _tempVacancies--;
        }

        private bool CanSave(object obj)
        {
            return SelectedPrisoners.Any();
        }

        private void Save(object obj)
        {
            _commands.ForEach(x => x.Execute());
            _cellDataService.UpdateCell(_cell);
            CellFlyweight.GetInstance.GetPrisonersFromCell(_cell);

            Data.SaveCellFlyweight();
            Messenger.Default.Send(new UpdateListMessage("Cell", _cell.Id));
        }

        private void OnCellReceived(Cell cell)
        {
            _cell = cell;
            _tempVacancies = cell.SpaceLeft();
            SelectedPrisoners = new ObservableCollection<Prisoner>();
            _commands = new List<ICellCommand>();
            LoadData();
        }

        private void LoadData()
        {
            Prisoners = CellFlyweight.GetInstance.GetFreePrisoners().ToObservableCollection();
        }

        public ICommand SaveCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand RemoveCommand { get; set; }


        private ObservableCollection<Prisoner> _prisoners;
        public ObservableCollection<Prisoner> Prisoners
        {
            get { return _prisoners; }
            set
            {
                if (_prisoners != value)
                {
                    _prisoners = value;
                    OnPropertyChanged("Prisoners");
                }
            }
        }

        private ObservableCollection<Prisoner> _selectedPrisoners;
        public ObservableCollection<Prisoner> SelectedPrisoners
        {
            get { return _selectedPrisoners; }
            set
            {
                if (_selectedPrisoners != value)
                {
                    _selectedPrisoners = value;
                    OnPropertyChanged("SelectedPrisoners");
                }
            }
        }
        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
