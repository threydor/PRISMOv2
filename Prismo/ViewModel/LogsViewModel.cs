﻿using Prismo.Model;
using Prismo.Model.PersonalLogger;
using Prismo.Utility;
using Prismo.Utility.Logger;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class LogsViewModel : INotifyPropertyChanged
    {
        private readonly DialogService _dialogService = new DialogService();

        public ICommand DeleteLogsCommand { get; set; }

        private string _mainLogs;
        public string MainLogs
        {
            get { return _mainLogs; }
            set
            {
                _mainLogs = value;
                OnPropertyChanged(nameof(MainLogs));
            }
        }

        private string _personalLogs;
        public string PersonalLogs
        {
            get { return _personalLogs; }
            set
            {
                _personalLogs = value;
                OnPropertyChanged(nameof(PersonalLogs));
            }
        }

        public LogsViewModel()
        {
            DeleteLogsCommand = new RelayCommand(DeleteLogs, CanDeleteLogs);

            Messenger.Default.Register<UpdateListMessage>(this, OnUpdateLogsMessageReceived);
        }
        
        private void OnUpdateLogsMessageReceived(UpdateListMessage obj)
        {
            LoadLogs();
        }

        private bool CanDeleteLogs(object obj)
        {
            return App.LoggedInEmployee.Perms == UserPerms.Admin;
        }
        private void DeleteLogs(object obj)
        {
            var messageBoxResult = MessageBox.Show("Czy na pewno chcesz usunąć wszystkie logi?", "Delete Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Logger.GetInstance.ClearLogs();
                new LogPersonAction(App.LoggedInEmployee).ClearLogHistory();
                LoadLogs();
            }
        }

        private void LoadLogs()
        {
            if (Logger.GetInstance.FileExists() && App.LoggedInEmployee != null)
            {
                MainLogs = File.ReadAllText(Data.DataBasePath + "\\logs.txt");

                var personalLogger = new LogPersonAction(App.LoggedInEmployee);
                var builder = new StringBuilder();
                foreach (var log in personalLogger.GetPersonalLog())
                {
                    builder.Append(log + "\n");
                }
                var result = builder.ToString();
                PersonalLogs = result;
            }
            else
            {
                Logger.GetInstance.SaveLogs();
            }
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
