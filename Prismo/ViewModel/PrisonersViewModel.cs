﻿using Prismo.Extensions;
using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using Prismo.Utility.Service;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class PrisonersViewModel : INotifyPropertyChanged
    {
        private readonly DialogService _dialogService = new DialogService();
        private IPrisonerDataService _prisonerDataService = new PrisonerDataService();

        public Visibility FilterPanelVisibility { get; set; }

        private Visibility _prisonerProfileVisibility;

        public Visibility PrisonerProfileVisibility
        {
            get { return _prisonerProfileVisibility; }
            set
            {
                _prisonerProfileVisibility = value;
                OnPropertyChanged(nameof(PrisonerProfileVisibility));
            }
        }

        public ICommand AddPrisonerCommand { get; set; }
        public ICommand EditPrisonerCommand { get; set; }
        public ICommand DeletePrisonerCommand { get; set; }
        public ICommand ChangePrisonersCellCommand { get; set; }
        public ICommand PrintPrisonersProfileCommand { get; set; }
        public ICommand ToggleFilterPanelCommand { get; set; }
        public ICommand FilterPrisonersCommand { get; set; }
        public ICommand CancelFilterCommand { get; set; }

        private ObservableCollection<Prisoner> _prisoners;
        public ObservableCollection<Prisoner> Prisoners
        {
            get { return _prisoners; }
            set
            {
                _prisoners = value;
                OnPropertyChanged("Prisoners");
            }
        }

        private Prisoner _selectedPrisoner;
        public Prisoner SelectedPrisoner
        {
            get { return _selectedPrisoner; }
            set
            {
                _selectedPrisoner = value;
                OnPropertyChanged(nameof(SelectedPrisoner));
                LoadPrisoner();
            }
        }

        private string _prisonersCell;

        public string PrisonersCell
        {
            get { return _prisonersCell; }
            set
            {
                _prisonersCell = value;
                OnPropertyChanged(nameof(PrisonersCell));
            }
        }

        private void LoadPrisoner()
        {
            if (SelectedPrisoner != null)
            {
                var cela = CellFlyweight.GetInstance.GetCellByPrisoner(SelectedPrisoner.Id);
                PrisonersCell = cela == null ? "brak celi" : $"{cela.Id} [w {cela.BlockName}]";
                PrisonerProfileVisibility = Visibility.Visible;
            }
            else
            {
                PrisonerProfileVisibility = Visibility.Collapsed;
            }
        }

        private string _searchID;
        public string SearchID
        {
            get { return _searchID; }
            set
            {
                _searchID = value;
                OnPropertyChanged(nameof(SearchID));
            }
        }

        private string _searchName;
        public string SearchName
        {
            get { return _searchName; }
            set
            {
                _searchName = value;
                OnPropertyChanged(nameof(SearchName));
            }
        }

        private string _searchSurname;
        public string SearchSurname
        {
            get { return _searchSurname; }
            set
            {
                _searchSurname = value;
                OnPropertyChanged(nameof(SearchSurname));
            }
        }

        public PrisonersViewModel()
        {
            AddPrisonerCommand = new RelayCommand(AddPrisoner, CanExecute);
            EditPrisonerCommand = new RelayCommand(EditPrisoner, CanEditOrRemovePrisoner);
            DeletePrisonerCommand = new RelayCommand(DeletePrisoner, CanEditOrRemovePrisoner);
            ChangePrisonersCellCommand = new RelayCommand(ChangePrisonersCell, CanEditOrRemovePrisoner);
            PrintPrisonersProfileCommand = new RelayCommand(PrintPrisonersProfile, CanEditOrRemovePrisoner);
            ToggleFilterPanelCommand = new RelayCommand(ToggleFilterPanel, CanToggleFilterPanel);
            FilterPrisonersCommand = new RelayCommand(FilterPrisoners, CanFilter);
            CancelFilterCommand = new RelayCommand(CancelFilter, CanExecute);

            FilterPanelVisibility = Visibility.Collapsed;
            PrisonerProfileVisibility = Visibility.Collapsed;
            OnPropertyChanged(nameof(this.FilterPanelVisibility));

            LoadData();
            Messenger.Default.Register<UpdateListMessage>(this, OnUpdateListMessageReceived);
        }

        private void DeletePrisoner(object obj)
        {
            var messageBoxResult = MessageBox.Show("Czy na pewno chcesz usunąć tego więźnia?", "Delete Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                _prisonerDataService.DeletePrisoner(SelectedPrisoner);
                Logger.GetInstance.AddPersonLog(" (" + App.LoggedInEmployee.Id + ") usunął więźnia: " + SelectedPrisoner.Name + " " + SelectedPrisoner.Surname);
                LoadData();
            }
        }

        private bool CanEditOrRemovePrisoner(object obj)
        {
            if (SelectedPrisoner != null)
            {
                return true;
            }
            return false;
        }

        private void OnUpdateListMessageReceived(UpdateListMessage obj)
        {
            OnPropertyChanged(nameof(SelectedPrisoner));
            LoadData();
            _dialogService.ClosePrisonerDialog();
            _dialogService.CloseChangeCellDialog();
        }

        private void LoadData()
        {
            Prisoners = _prisonerDataService.GetAllPrisoners().ToObservableCollection();
            LoadPrisoner();
        }
        private bool CanFilter(object obj)
        {
            return !string.IsNullOrEmpty(SearchID) || !string.IsNullOrEmpty(SearchName) || !string.IsNullOrEmpty(SearchSurname);
        }
        private void FilterPrisoners(object obj)
        {
            var idFilter = 0;
            int.TryParse(SearchID, out idFilter);

            var nameFilter = SearchName;
            var surnameFilter = SearchSurname;

            LoadData();
            var result = Prisoners.ToList();

            if (idFilter != 0)
                result = result.Where(x => x.Id == idFilter).ToList();

            if (nameFilter != null)
                result = result.Where(x => x.Name.ToLower().Contains(nameFilter.ToLower())).ToList();

            if (surnameFilter != null)
                result = result.Where(x => x.Surname.ToLower().Contains(surnameFilter)).ToList();

            Prisoners = result.ToObservableCollection();
        }

        private void PrintPrisonersProfile(object obj)
        {
            if (SelectedPrisoner != null)
            {
                SelectedPrisoner.Print();
                Logger.GetInstance.AddPersonLog(" (" + App.LoggedInEmployee.Id + ") wydrukował kartę więźnia: " + SelectedPrisoner.Name + " " + SelectedPrisoner.Surname);
            }
        }

        private void ChangePrisonersCell(object obj)
        {
            Messenger.Default.Send(SelectedPrisoner);
            _dialogService.ShowChangeCellDialog();
        }

        private void EditPrisoner(object obj)
        {
            Messenger.Default.Send(SelectedPrisoner);
            _dialogService.ShowPrisonerDialog();
        }

        private void AddPrisoner(object obj)
        {
            Messenger.Default.Send(new Prisoner());
            _dialogService.ShowPrisonerDialog();
        }

        private bool CantExecute(object obj) => false;
        private bool CanExecute(object obj) => true;


        private bool CanToggleFilterPanel(object obj)
        {
            return FilterPanelVisibility != Visibility.Visible;
        }

        private void CancelFilter(object obj)
        {
            this.FilterPanelVisibility = Visibility.Collapsed;
            OnPropertyChanged(nameof(this.FilterPanelVisibility));
            LoadData();
            SearchID = null;
            SearchName = null;
            SearchSurname = null;
        }

        private void ToggleFilterPanel(object obj)
        {
            this.FilterPanelVisibility = Visibility.Visible;
            OnPropertyChanged(nameof(this.FilterPanelVisibility));
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
