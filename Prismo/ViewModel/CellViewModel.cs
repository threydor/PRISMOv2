﻿using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using Prismo.Utility.Service;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class CellViewModel : INotifyPropertyChanged
    {
        private Cell _selectedCell;
        public int SelectedCellSize { get; set; }

        public ICommand SaveCommand { get; set; }
        public CellViewModel()
        {
            Messenger.Default.Register<Cell>(this, OnCellReceived);
            SaveCommand = new RelayCommand(Save, CanSaveCell);
        }
        private bool CanSaveCell(object obj)
        {
            return SelectedCellSize > 0;
        }

        private void Save(object obj)
        {
            if (Data.CellsList.Any(x => x.Id == _selectedCell.Id))
            {
                if (_selectedCell.Size < _selectedCell.Population)
                {
                    var msg = $"Przypisana liczba więźniów: {_selectedCell.Population}. Wprowadź większą liczbę wolnych miejsc.";
                    MessageBox.Show(msg, "Informacja", MessageBoxButton.OK);
                    return;
                }
                _selectedCell.Size = SelectedCellSize;
                Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) edytował celę: {_selectedCell.Id}, miejsc: {_selectedCell.Size}");
                new CellDataService().UpdateCell(_selectedCell);
            }
            else
            {
                _selectedCell.Size = SelectedCellSize;
                Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) dodał celę: {_selectedCell.Id}, miejsc: {_selectedCell.Size}");
                Data.AddCell(_selectedCell);
                Data.SaveCells();
            }
            Messenger.Default.Send(new UpdateListMessage("Cell", _selectedCell.Id));
        }

        public Cell SelectedCell
        {
            get { return _selectedCell; }
            set
            {
                if (_selectedCell != value)
                {
                    _selectedCell = value;
                    OnPropertyChanged("SelectedCell");
                }
            }
        }

        private void OnCellReceived(Cell cell)
        {
            SelectedCell = cell;
            SelectedCellSize = cell.Size;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
