﻿using Prismo.Utility;
using Prismo.Utility.Logger;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private object _selectedViewModel;
        public object SelectedViewModel
        {
            get { return _selectedViewModel; }
            set
            {
                _selectedViewModel = value;
                OnPropertyChanged("SelectedViewModel");
                MenuVisibility = Visibility.Collapsed;
            }
        }

        public Visibility ShowMenuBtnVisibility { get; set; }

        private Visibility _menuVisibility;
        public Visibility MenuVisibility
        {
            get { return _menuVisibility; }
            set
            {
                _menuVisibility = value;
                OnPropertyChanged("MenuVisibility");
            }
        }

        public Visibility LoginStackPanelVisibility { get; set; }

        public string LoggedInEmployee
        {
            get { return App.LoggedInEmployee != null ? App.LoggedInEmployee.ToString() : "-"; }
        }

        public ICommand NavigateCommand { get; set; }


        public MainWindowViewModel()
        {
            Logger.GetInstance.AddLog("Application starting.");
            Data.LoadCellFlyweight();
            //Data.LoadBlocks();
            //Data.LoadCells();

            NavigateCommand = new RelayCommand(Navigate, CanNavigate);

            if (App.LoggedInEmployee == null)
            {
                SelectedViewModel = new LoginViewModel();
            }

            ShowMenuBtnVisibility = Visibility.Hidden;
            OnPropertyChanged("ShowMenuBtnVisibility");
        }

        private bool CanNavigate(object obj)
        {
            var parameter = obj.ToString();
            if ((parameter == "LoggedIn" || parameter == "ShowMenu") && (App.LoggedInEmployee == null || MenuVisibility == Visibility.Visible))
            {
                return false;
            }
            return true;
        }

        private void Navigate(object parameter)
        {
            switch (parameter.ToString())
            {
                case "LoggedIn":
                    SelectedViewModel = null;

                    MenuVisibility = Visibility.Visible;
                    ShowMenuBtnVisibility = Visibility.Visible;
                    OnPropertyChanged("ShowMenuBtnVisibility");
                    LoginStackPanelVisibility = Visibility.Hidden;
                    OnPropertyChanged("LoginStackPanelVisibility");

                    OnPropertyChanged("LoggedInEmployee");
                    break;

                case "ShowMenu":
                    SelectedViewModel = null;
                    MenuVisibility = Visibility.Visible;
                    break;
                    
                case "PrisonViewModel":
                    SelectedViewModel = new PrisonViewModel();
                    Messenger.Default.Send(new UpdateListMessage());
                    break;

                case "PrisonersViewModel":
                    SelectedViewModel = new PrisonersViewModel();
                    Messenger.Default.Send(new UpdateListMessage());
                    break;

                case "LogsViewModel":
                    SelectedViewModel = new LogsViewModel();
                    Messenger.Default.Send(new UpdateListMessage());
                    break;

                case "UserProfile":
                    SelectedViewModel = new UserProfileViewModel();
                    break;
            }
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
