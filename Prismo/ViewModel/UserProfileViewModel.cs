﻿using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using Prismo.Utility.Service;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class UserProfileViewModel : INotifyPropertyChanged
    {
        private readonly IUserDataService _userDataService = new UserDataService();

        public int Id
        {
            get { return App.LoggedInEmployee.Id; }
        }

        public string LastName
        {
            get { return App.LoggedInEmployee.LastName; }
        }

        public UserPerms Perms
        {
            get { return App.LoggedInEmployee.Perms; }
        }

        private Visibility _changePasswordPanelVisibility;
        public Visibility ChangePasswordPanelVisibility
        {
            get { return _changePasswordPanelVisibility; }
            set
            {
                _changePasswordPanelVisibility = value;
                OnPropertyChanged(nameof(ChangePasswordPanelVisibility));
                OnPropertyChanged(nameof(ChangePasswordBtnVisibility));
            }
        }

        public Visibility ChangePasswordBtnVisibility
        {
            get
            {
                return ChangePasswordPanelVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public ICommand ChangePasswordCommand { get; set; }
        public ICommand SaveNewPassword { get; set; }

        public UserProfileViewModel()
        {
            ChangePasswordCommand = new RelayCommand(ChangePassword, CanChangePass);
            SaveNewPassword = new RelayCommand(SavePassword, CanSavePassword);

            ChangePasswordPanelVisibility = Visibility.Collapsed;
        }

        private bool CanSavePassword(object obj)
        {
            var passwordBox = obj as PasswordBox;
            return passwordBox != null && !string.IsNullOrEmpty(passwordBox.Password);
        }

        private void SavePassword(object obj)
        {
            var userToEdit = App.LoggedInEmployee;
            var userToSave = new UserBuilder(userToEdit);

            Logger.GetInstance.AddPersonLog(" (" + App.LoggedInEmployee.Id + ") " + App.LoggedInEmployee.LastName + "zmienił swoje hasło.");

            using (var md5Hash = MD5.Create())
            {
                var passwordBox = obj as PasswordBox;
                var pass = GetMd5Hash(md5Hash, passwordBox.Password);

                userToSave.Password(pass);
            }

            _userDataService.AddOrUpdateUser((User)userToSave.Build());
            ChangePasswordPanelVisibility = Visibility.Collapsed;
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();

            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }

        private bool CanChangePass(object obj) => true;
        private void ChangePassword(object obj)
        {
            ChangePasswordPanelVisibility = Visibility.Visible;
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
