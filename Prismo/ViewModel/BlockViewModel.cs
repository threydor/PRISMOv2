﻿using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class BlockViewModel : INotifyPropertyChanged
    {
        private Cellblock _selectedBlock;

        public BlockViewModel()
        {
            Messenger.Default.Register<Cellblock>(this, OnBlockReceived);
            SaveCommand = new RelayCommand(SaveBlock, CanSaveBlock);
            CancelCommand = new RelayCommand(CancelBlock, CanCancel);
        }

        private bool CanCancel(object obj)
        {
            return true;
        }

        private void CancelBlock(object obj)
        {
            Messenger.Default.Send(new UpdateListMessage());
        }

        private bool CanSaveBlock(object obj)
        {
            return !string.IsNullOrEmpty(_selectedBlock.Name);
        }

        private void SaveBlock(object obj)
        {
            if (Data.BlocksList.FirstOrDefault(x => x.Id == _selectedBlock.Id) != null)
            {
                Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) edytował blok: [{_selectedBlock.Id}] {_selectedBlock.Name} ({_selectedBlock.Type})");
                Data.UpdateBlock(_selectedBlock);
            }
            else
            {
                Logger.GetInstance.AddPersonLog($" ({App.LoggedInEmployee.Id}) dodał block: [{_selectedBlock.Id}] {_selectedBlock.Name} ({_selectedBlock.Type})");
                Data.AddBlock(_selectedBlock);
                Data.SaveBlocks();
            }

            Messenger.Default.Send(new UpdateListMessage("Block", _selectedBlock.Id));
        }

        public ICommand CancelCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public Cellblock SelectedBlock
        {
            get { return _selectedBlock; }
            set
            {
                if (_selectedBlock != value)
                {
                    _selectedBlock = value;
                    OnPropertyChanged("SelectedBlock");
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnBlockReceived(Cellblock block)
        {
            SelectedBlock = block;
        }
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
