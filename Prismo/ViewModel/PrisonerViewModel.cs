﻿using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using Prismo.Utility.Service;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class PrisonerViewModel : INotifyPropertyChanged
    {
        private IPrisonerDataService _prisonerDataService = new PrisonerDataService();
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime AdmissionDate { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Height { get; set; }
        public string Photo { get; set; }
        public string Remarks { get; set; }
        public string Eyes { get; set; }
        public string Hair { get; set; }
        
        public ICommand CancelCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        public PrisonerViewModel()
        {
            Messenger.Default.Register<Prisoner>(this, OnPrisonerReceived);
            SaveCommand = new RelayCommand(SavePrisoner, CanSavePrisoner);
            CancelCommand = new RelayCommand(CancelPrisoner, CanCancel);
        }

        private bool CanCancel(object obj)
        {
            return true;
        }

        private void CancelPrisoner(object obj)
        {
            Messenger.Default.Send(new UpdateListMessage());
        }

        private bool CanSavePrisoner(object obj)
        {
            if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(Surname))
            {
                return false;
            }
            return true;
        }

        private void SavePrisoner(object obj)
        {
            var prisonerToEdit = _prisonerDataService.GetPrisonerById(Id);
            PrisonerBuilder prisonerToSave;

            if (prisonerToEdit == null)
            {
                prisonerToSave = new PrisonerBuilder();
                Logger.GetInstance.AddPersonLog(" (" + App.LoggedInEmployee.Id + ") dodał więźnia: " + Name + " " + Surname);

            }
            else
            {
                prisonerToSave = new PrisonerBuilder(prisonerToEdit);
                Logger.GetInstance.AddPersonLog(" (" + App.LoggedInEmployee.Id + ") edytował więźnia: " + Name + " " + Surname);

            }

            prisonerToSave
                .Name(Name)
                .Surname(Surname)
                .AdmissionDate(AdmissionDate)
                .ReleaseDate(ReleaseDate)
                .DateOfBirth(DateOfBirth)
                .Height(Height)
                .Hair(Hair)
                .Eyes(Eyes)
                .Remarks(Remarks);

            _prisonerDataService.AddOrUpdatePrisoner((Prisoner)prisonerToSave.Build());
            Messenger.Default.Send(new UpdateListMessage());
        }

        private void OnPrisonerReceived(Prisoner prisoner)
        {
            this.Id = prisoner.Id;
            this.Name = prisoner.Name;
            this.Surname = prisoner.Surname;
            this.AdmissionDate = prisoner.AdmissionDate;
            this.ReleaseDate = prisoner.ReleaseDate;
            this.DateOfBirth = prisoner.DateOfBirth;
            this.Height = prisoner.Height;
            this.Hair = prisoner.Hair;
            this.Eyes = prisoner.Eyes;
            this.Remarks = prisoner.Remarks;
        }
        
        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
