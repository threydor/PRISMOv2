﻿using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using Prismo.Utility.Service;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class UserViewModel : INotifyPropertyChanged
    {
        private IUserDataService _userDataService = new UserDataService();
        // public User User { get; set; }
        public int Id { get; set; }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged(nameof(LastName));
            }
        }

        private UserPerms _perms;
        public UserPerms Perms
        {
            get { return _perms; }
            set
            {
                _perms = value;
                OnPropertyChanged(nameof(Perms));
            }
        }

        public ICommand CancelCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        public UserViewModel()
        {
            Messenger.Default.Register<User>(this, OnUserReceiver);
            SaveCommand = new RelayCommand(SaveUser, CanSaveUser);
            CancelCommand = new RelayCommand(Cancel, CanCancel);
        }

        private bool CanCancel(object obj)
        {
            return true;
        }

        private void Cancel(object obj)
        {
            Messenger.Default.Send(new UpdateListMessage());
        }

        private bool CanSaveUser(object obj)
        {
            var passwordBox = obj as PasswordBox;
            if (passwordBox == null || string.IsNullOrEmpty(LastName) || string.IsNullOrEmpty(passwordBox.Password))
                return false;
            return true;
        }

        private void SaveUser(object obj)
        {
            var userToEdit = _userDataService.GetUserById(Id);
            UserBuilder userToSave;

            if (userToEdit == null)
            {
                userToSave = new UserBuilder();
                Logger.GetInstance.AddPersonLog(" (" + App.LoggedInEmployee.Id + ") dodał użytkownika: " + Id + " " + LastName);
            }
            else
            {
                userToSave = new UserBuilder(userToEdit);
                Logger.GetInstance.AddPersonLog(" (" + App.LoggedInEmployee.Id + ") edytował użytkownika: " + Id + " " + LastName);
            }

            using (MD5 md5Hash = MD5.Create())
            {
                var passwordBox = obj as PasswordBox;
                var pass = GetMd5Hash(md5Hash, passwordBox.Password);

                userToSave
                    .LastName(LastName)
                    .Password(pass)
                    .Perms(Perms);
            }

            _userDataService.AddOrUpdateUser((User)userToSave.Build());
            Messenger.Default.Send(new UpdateListMessage());
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();

            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }


        private void OnUserReceiver(User user)
        {
            this.Id = user.Id;
            this.LastName = user.LastName;
            this.Perms = user.Perms;
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
