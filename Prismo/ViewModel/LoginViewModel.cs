﻿using Prismo.Model;
using Prismo.Utility;
using Prismo.Utility.Logger;
using Prismo.Utility.Service;
using System;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Prismo.ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private IUserDataService _userDataService;

        public string LoginAlert { get; set; }
        public string Id { get; set; }

        public Visibility LoginGridVisibility { get; set; }

        public ICommand LoginCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public LoginViewModel()
        {
            Data.PrepareDatabase();

            LoginCommand = new RelayCommand(Login, CanLogin);
            CancelCommand = new RelayCommand(Cancel, CanCancel);

            LoginGridVisibility = Visibility.Visible;

            _userDataService = new UserDataService();
        }

        private bool CanLogin(object obj)
        {
            var passwordBox = obj as PasswordBox;
            int id;
            int.TryParse(Id, out id);
            return !string.IsNullOrEmpty(Id) && !string.IsNullOrEmpty(passwordBox.Password) && id != 0;
        }

        private void Login(object obj)
        {
            if (Data.IsNewDatabase())
            {
                CreateAdminAccount(obj);
            }

            using (MD5 md5Hash = MD5.Create())
            {
                var usersList = _userDataService.GetAllUsers();

                int id;
                int.TryParse(Id, out id);

                var passwordBox = obj as PasswordBox;
                var pass = passwordBox.Password;

                var foundAccount = usersList.FirstOrDefault(u => u.Id == id);
                if (foundAccount == null)
                {
                    var info = "Couldn't find any account matching entered credentials!";
                    Logger.GetInstance.AddLog(info);
                    Alert(info);
                    return;
                }
                if (foundAccount.Perms == UserPerms.Inactive)
                {
                    var info = "Your account has been suspended!";
                    Logger.GetInstance.AddLog(info);
                    Alert(info);
                    return;
                }

                if (VerifyMd5Hash(md5Hash, pass, foundAccount.Password))
                {
                    App.LoggedInEmployee = foundAccount;
                    LoginGridVisibility = Visibility.Collapsed;
                    OnPropertyChanged("LoginGridVisibility");

                    var info = $"{foundAccount.LastName} ({foundAccount.Id}) logged in to Prismo.";
                    Logger.GetInstance.AddPersonLog(info);
                }
                else
                {
                    var info = "Couldn't find any account matching entered credentials!";
                    Logger.GetInstance.AddLog(info);
                    Alert(info);
                }
            }
        }

        private void CreateAdminAccount(object obj)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                int id;
                int.TryParse(Id, out id);

                var passwordBox = obj as PasswordBox;
                var pass = GetMd5Hash(md5Hash, passwordBox.Password);

                var newAccount = new UserBuilder()
                    .Id(id)
                    .Password(pass)
                    .Perms(UserPerms.Admin)
                    .Build();

                _userDataService.AddUser((User)newAccount);

                var info = $"Created new admin account for user: {((User)newAccount).LastName} (id: {((User)newAccount).Id})";
                Logger.GetInstance.AddLog(info);
            }
        }

        private void Alert(string msg)
        {
            LoginAlert = msg;
            OnPropertyChanged("LoginAlert");
        }

        #region MD5

        private bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            var hashOfInput = GetMd5Hash(md5Hash, input);
            var comparer = StringComparer.OrdinalIgnoreCase;

            return 0 == comparer.Compare(hashOfInput, hash);
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();

            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }

        #endregion

        private void Cancel(object obj)
        {
            Application.Current.Shutdown();
        }

        private bool CanCancel(object obj) => true;

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}