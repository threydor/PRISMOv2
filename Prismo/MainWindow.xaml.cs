﻿using Prismo.Utility;
using Prismo.Utility.Logger;
using System.ComponentModel;

namespace Prismo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = ViewModelLocator.MainWindowViewModel;
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            Data.SaveCellFlyweight();
            Logger.GetInstance.AddLog("Application shutdown.");
            Logger.GetInstance.SaveLogs();
        }
    }
}
