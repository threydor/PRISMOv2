﻿using System.Collections.Generic;

namespace Prismo.Model
{
    public class Cellblock
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CellblockType Type { get; set; }
        public List<Cell> Cells { get; set; }

        public Cellblock()
        {
            Cells = new List<Cell>();
        }
        public override string ToString()
        {
            return Name + " [ " + Type + " ]";
        }
    }

    public enum CellblockType
    {
        Zwykły,
        Zaostrzony
    }
}
