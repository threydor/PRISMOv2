﻿namespace Prismo.Model
{
    public interface IPersonBuilder
    {
        Person Build();
    }
}