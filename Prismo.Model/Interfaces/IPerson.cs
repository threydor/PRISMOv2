﻿using System.Collections.Generic;

namespace Prismo.Model
{
    public interface IPerson
    {
        List<string> GetPersonalLog();
    }
}