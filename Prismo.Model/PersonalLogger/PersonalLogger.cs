﻿using System;
using System.Collections.Generic;

namespace Prismo.Model.PersonalLogger
{
    public class PersonalLogger : Person
    {
        public Person PersonalDecorator;
        public PersonalLogger(Person person)
        {
            this.PersonalDecorator = person;
        }
        public override List<String> GetPersonalLog()
        {
            return PersonalDecorator.GetPersonalLog();
        }

        protected override void PrepareRecordsTemplate()
        {
            throw new NotImplementedException();
        }
    }
}
