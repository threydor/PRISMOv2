﻿using System.Collections.Generic;

namespace Prismo.Model.PersonalLogger
{
    public class LogPersonAction : PersonalLogger
    {
        protected static List<ActionLog> logHistory = new List<ActionLog>();
        public LogPersonAction(Person person) : base(person)
        {
        }

        public void AddNewLog(ActionLog log)
        {
            logHistory.Add(log);
        }
        public override List<string> GetPersonalLog()
        {
            var result = base.PersonalDecorator.GetPersonalLog();
            foreach(var log in logHistory)
            {
                result.Add(log.ToString());
            }
            return result;
        }
        public void ClearLogHistory()
        {
           logHistory.Clear();
        }
    }
}
