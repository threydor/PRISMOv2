﻿using System;

namespace Prismo.Model.PersonalLogger
{
    public class ActionLog
    {
        private DateTime _date;
        private string _info;

        public ActionLog(string info)
        {
            _info = info;
            _date = DateTime.Now;
        }

        public override string ToString()
        {
            var format = "yyyy-MM-dd HH:mm:ss";
            return $"{_date.ToString(format)} {_info}";
        }
    }
}