﻿namespace Prismo.Model
{
    public enum UserPerms
    {
        Inactive,
        Active,
        Admin
    }
}
