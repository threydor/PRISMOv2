﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Prismo.Model
{
    public class Prisoner : Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime AdmissionDate { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Height { get; set; }
        public string Hair { get; set; }
        public string Eyes { get; set; }
        public string Remarks { get; set; }

        public Prisoner()
        {
            AdmissionDate = DateTime.Now;
            ReleaseDate = DateTime.Now.AddDays(1);
            DateOfBirth = new DateTime(1901, 01, 01);
        }

        public override List<string> GetPersonalLog()
        {
            return BuildPersonalLog();
        }

        private List<string> BuildPersonalLog()
        {
            var result = new List<string>();
            result.Add("Imie:" + this.Name);
            result.Add("Nazwisko:" + this.Surname);
            result.Add("Data przyjęcia: " + this.AdmissionDate.ToShortDateString());
            result.Add("Data wyjścia: " + this.ReleaseDate.ToShortDateString());
            result.Add("Data urodzenia: " + this.DateOfBirth.ToShortDateString());
            result.Add("Wzrost" + this.Height.ToString());
            result.Add("Kolor włosów:" + this.Hair);
            return result;
        }
        protected override void PrepareRecordsTemplate()
        {
            using (StreamWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "cfg\\recordstemplate.xml"))
            {
                writer.WriteLine("<FlowDocument xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x = \"http://schemas.microsoft.com/winfx/2006/xaml\">");
                writer.WriteLine("<Paragraph FontFamily=\"Arial\">");
                writer.WriteLine("<StackPanel Orientation=\"Vertical\" Margin=\"0 0 0 0\">");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Margin=\"30 0 0 0\" FontSize=\"50\" Text = \"KARTOTEKA\" VerticalAlignment = \"Center\" /> ");
                writer.WriteLine("<Grid Grid.Row=\"0\" Margin=\"15, 5\">");
                writer.WriteLine("<Grid.RowDefinitions>");
                for (int i = 0; i < 12; i++)
                {
                    writer.WriteLine("<RowDefinition Height=\"*\" />");
                }
                writer.WriteLine("</Grid.RowDefinitions>");
                writer.WriteLine("<Grid.ColumnDefinitions>");
                writer.WriteLine("<ColumnDefinition Width=\"2*\" />");
                writer.WriteLine("<ColumnDefinition Width=\"*\" />");
                writer.WriteLine("<ColumnDefinition Width=\"*\" />");
                writer.WriteLine("</Grid.ColumnDefinitions>");

                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"0\" HorizontalAlignment=\"Right\" Text =\"Imię: \" FontWeight=\"Bold\"/>");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"1\" HorizontalAlignment=\"Right\" Text =\"Nazwisko: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"2\" HorizontalAlignment=\"Right\" Text =\"Data urodzenia: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"3\" HorizontalAlignment=\"Right\" Text =\"Kolor włosów: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"4\" HorizontalAlignment=\"Right\" Text =\"Kolor oczu: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"5\" HorizontalAlignment=\"Right\" Text =\"Wzrost: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"6\" HorizontalAlignment=\"Right\" Text =\"Data przyjęcia: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"7\" HorizontalAlignment=\"Right\" Text =\"Koniec wyroku: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"8\" HorizontalAlignment=\"Right\" Text =\"Uwagi: \" FontWeight=\"Bold\" />");


                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Name,Mode=OneWay}\" Grid.Row=\"0\" Grid.Column=\"1\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Path=Surname,Mode=TwoWay}\" Grid.Row=\"1\" Grid.Column=\"1\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding DateOfBirth, StringFormat={}{0:dd/MM/yyyy}}\" Grid.Row=\"2\" Grid.Column=\"1\"/>");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Hair}\" Grid.Row=\"3\" Grid.Column=\"1\"/>");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Eyes}\" Grid.Row=\"4\" Grid.Column=\"1\"/>");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Height}\" Grid.Row=\"5\" Grid.Column=\"1\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding AdmissionDate, StringFormat={}{0:dd/MM/yyyy}}\" Grid.Row=\"6\" Grid.Column=\"1\"/>");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding ReleaseDate, StringFormat={}{0:dd/MM/yyyy}}\" Grid.Row=\"7\" Grid.Column=\"1\"/>");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Remarks}\" Grid.Row=\"8\" Grid.Column=\"1\" />");
                writer.WriteLine("<Image Name=\"Image\"  Grid.Column=\"2\" Grid.Row=\"0\" Grid.RowSpan=\"4\" Source=\"{Binding Photo}\"/>");
                writer.WriteLine("</Grid>");
                writer.WriteLine("</StackPanel>");
                writer.WriteLine("</Paragraph>");
                writer.WriteLine("</FlowDocument>");
            }
        }
    }
}