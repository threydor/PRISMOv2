﻿using System.Collections.Generic;

namespace Prismo.Model
{
    public class Cell
    {
        public int Id { get; set; }
        public int BlockId { get; set; }
        public int Size { get; set; }
        public string BlockName { get; set; }
        public int Population { get; set; }
        public Cell(int size)
        {
            Size = size;
        }
        public bool IsPopulated()
        {
            return (Population > 0);
        }
        public bool HasFreeSpace()
        {
            return (SpaceLeft() > 0);
        }
        public void AddNewPrisoner(Prisoner prisoner)
        {
        }
        public void RemovePrisoner(Prisoner prisoner)
        {
        }
        public int SpaceLeft()
        {
            return (Size - Population);
        }
    }
}
