﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Printing;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps;
using System.Xml;

namespace Prismo.Model
{
    public abstract class Person : IPerson
    {
        public Person()
        {
        }
        public abstract List<string> GetPersonalLog();
        protected abstract void PrepareRecordsTemplate();
        public void Print()
        {
            PrepareRecordsTemplate();
            IDocumentPaginatorSource flowDocument =
               RenderFlowDocumentTemplate(AppDomain.CurrentDomain.BaseDirectory + "cfg//recordstemplate.xml", this);

            PrintDialog dlg = new PrintDialog();

            if (dlg.ShowDialog() == false)
                return;
            PrintQueue flowPrintQueue = dlg.PrintQueue;
            PrintFlowDocument(flowPrintQueue, flowDocument.DocumentPaginator);
        }
        public IDocumentPaginatorSource RenderFlowDocumentTemplate(string templatePath, object dataContextObject)
        {
            string rawXamlText = "";
            using (StreamReader streamReader = File.OpenText(templatePath))
            {
                rawXamlText = streamReader.ReadToEnd();
            }

            FlowDocument document = XamlReader.Load(new XmlTextReader(new StringReader(rawXamlText))) as FlowDocument;
            document.DataContext = dataContextObject;

            return document;
        }


        public void PrintFlowDocument(PrintQueue printQueue, DocumentPaginator document)
        {
            XpsDocumentWriter xpsDocumentWriter = PrintQueue.CreateXpsDocumentWriter(printQueue);
            xpsDocumentWriter.Write(document);
        }
    }
}