﻿using System;

namespace Prismo.Model
{
    public class PrisonerBuilder : IPersonBuilder
    {
        private Prisoner _prisoner;

        public PrisonerBuilder(Prisoner prisoner)
        {
            this._prisoner = prisoner;
        }

        public PrisonerBuilder()
        {
            _prisoner = new Prisoner();
        }

        public PrisonerBuilder Id(int id)
        {
            _prisoner.Id = id;
            return this;
        }

        public PrisonerBuilder Name(string name)
        {
            _prisoner.Name = name;
            return this;
        }

        public PrisonerBuilder Surname(string surname)
        {
            _prisoner.Surname = surname;
            return this;
        }

        public PrisonerBuilder AdmissionDate(DateTime admissionDate)
        {
            _prisoner.AdmissionDate = admissionDate;
            return this;
        }

        public PrisonerBuilder ReleaseDate(DateTime releaseDate)
        {
            _prisoner.ReleaseDate = releaseDate;
            return this;
        }

        public PrisonerBuilder DateOfBirth(DateTime dateOfBirth)
        {
            _prisoner.DateOfBirth = dateOfBirth;
            return this;
        }

        public PrisonerBuilder Height(int height)
        {
            _prisoner.Height = height;
            return this;
        }

        public PrisonerBuilder Hair(string hair)
        {
            _prisoner.Hair = hair;
            return this;
        }

        public PrisonerBuilder Eyes(string eyes)
        {
            _prisoner.Eyes = eyes;
            return this;
        }

        public PrisonerBuilder Remarks(string remarks)
        {
            _prisoner.Remarks = remarks;
            return this;
        }

        public Person Build()
        {
            return _prisoner;
        }
    }
}