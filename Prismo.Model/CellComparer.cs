﻿using System.Collections.Generic;

namespace Prismo.Model
{
    public class CellComparer : IEqualityComparer<Cell>
    {
        public bool Equals(Cell x, Cell y)
        {
            if (x.Id == y.Id && x.BlockName == y.BlockName && x.BlockId == y.BlockId)
                return true;

            return false;
        }

        public int GetHashCode(Cell obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
