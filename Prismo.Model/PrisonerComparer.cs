﻿using System.Collections.Generic;

namespace Prismo.Model
{
    public class PrisonerComparer : IEqualityComparer<Prisoner>
    {
        public bool Equals(Prisoner x, Prisoner y)
        {
            if (x.Id == y.Id)
                return true;

            return false;
        }

        public int GetHashCode(Prisoner obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}