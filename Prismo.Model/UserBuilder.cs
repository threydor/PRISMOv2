﻿namespace Prismo.Model
{
    public class UserBuilder : IPersonBuilder
    {
        private User _user;

        public UserBuilder(User user)
        {
            this._user = user;
        }

        public UserBuilder()
        {
            _user = new User();
        }

        public UserBuilder Id(int id)
        {
            _user.Id = id;
            return this;
        }

        public UserBuilder LastName(string lastName)
        {
            _user.LastName = lastName;
            return this;
        }

        public UserBuilder Password(string password)
        {
            _user.Password = password;
            return this;
        }

        public UserBuilder Perms(UserPerms perms)
        {
            _user.Perms = perms;
            return this;
        }

        public Person Build()
        {
            return _user;
        }
    }
}