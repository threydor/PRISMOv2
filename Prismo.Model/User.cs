﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Prismo.Model
{
    public class User : Person
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public UserPerms Perms { get; set; }

        public override string ToString()
        {
            return "ID: " + Id.ToString() + ", Last name: " + LastName + " | perms: " + Perms;
        }

        public override List<string> GetPersonalLog()
        {
            return BuildPersonalLog();
        }

        private List<string> BuildPersonalLog()
        {
            var result = new List<string>();
            result.Add("Id:" + this.Id);
            result.Add("Nazwisko:" + this.LastName);
            return result;
        }
        protected override void PrepareRecordsTemplate()
        {
            using (StreamWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "cfg\\recordstemplate.xml"))
            {
                writer.WriteLine("<FlowDocument xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xmlns:x = \"http://schemas.microsoft.com/winfx/2006/xaml\">");
                writer.WriteLine("<Paragraph FontFamily=\"Arial\">");
                writer.WriteLine("<StackPanel Orientation=\"Vertical\" Margin=\"0 0 0 0\">");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Margin=\"30 0 0 0\" FontSize=\"50\" Text = \"PROFIL\" VerticalAlignment = \"Center\" /> ");
                writer.WriteLine("<Grid Grid.Row=\"0\" Margin=\"15, 5\">");
                writer.WriteLine("<Grid.RowDefinitions>");
                for (int i = 0; i < 12; i++)
                {
                    writer.WriteLine("<RowDefinition Height=\"*\" />");
                }
                writer.WriteLine("</Grid.RowDefinitions>");
                writer.WriteLine("<Grid.ColumnDefinitions>");
                writer.WriteLine("<ColumnDefinition Width=\"2*\" />");
                writer.WriteLine("<ColumnDefinition Width=\"*\" />");
                writer.WriteLine("<ColumnDefinition Width=\"*\" />");
                writer.WriteLine("</Grid.ColumnDefinitions>");

                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"0\" HorizontalAlignment=\"Right\" Text =\"Imię: \" FontWeight=\"Bold\"/>");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"1\" HorizontalAlignment=\"Right\" Text =\"Nazwisko: \" FontWeight=\"Bold\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Grid.Row=\"2\" HorizontalAlignment=\"Right\" Text =\"Uprawnienia: \" FontWeight=\"Bold\" />");

                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Id,Mode=OneWay}\" Grid.Row=\"0\" Grid.Column=\"1\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding LastName,Mode=TwoWay}\" Grid.Row=\"1\" Grid.Column=\"1\" />");
                writer.WriteLine("<TextBlock Foreground=\"#000000\" Text=\"{Binding Perms}\" Grid.Row=\"2\" Grid.Column=\"1\"/>");
                writer.WriteLine("</Grid>");
                writer.WriteLine("</StackPanel>");
                writer.WriteLine("</Paragraph>");
                writer.WriteLine("</FlowDocument>");
            }
        }
    }
}
